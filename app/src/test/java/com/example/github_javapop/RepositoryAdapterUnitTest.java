package com.example.github_javapop;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;

import adapter.RepositoryAdapter;
import model.Repository;
import model.User;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

@RunWith(RobolectricTestRunner.class)
public class RepositoryAdapterUnitTest {
    private RepositoryAdapter adapter;
    private Context context;

    public void setUp() {
        context = RuntimeEnvironment.application;
    }

    @Test
    public void shouldAddListOnSetRepositorySuccess() {
        adapter = new RepositoryAdapter(context);
        List<Repository> repositoryList = new ArrayList<>();
        repositoryList.add(new Repository(
                6498492,
                "javascript",
                "JavaScript Style Guide",
                12175,
                29409,
                new User(
                        "ReactiveX",
                        "https://avatars1.githubusercontent.com/u/6407041?v=4"
                ),
                36
        ));

        adapter.setRepositories(repositoryList);
        Assert.assertEquals(1, adapter.getItemCount());
    }

    @Test
    public void shouldNotAddEmptyListRepository() {
        adapter = new RepositoryAdapter(context);
        List<Repository> repositoryList = new ArrayList<>();

        adapter.setRepositories(repositoryList);
        Assert.assertEquals(0, adapter.getItemCount());
    }

    @Test
    public void shouldNotAddNullListRepository() {
        adapter = new RepositoryAdapter(context);
        List<Repository> repositoryList = null;

        adapter.setRepositories(repositoryList);
        Assert.assertEquals(0, adapter.getItemCount());
    }
}
