package model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RepositoryResponse {
    @SerializedName("items")
    private List<Repository> repositories;

       public List<Repository> getRepositories() {
        return repositories;
    }

    public RepositoryResponse(List<Repository> repositories) {
        this.repositories = repositories;
    }
}