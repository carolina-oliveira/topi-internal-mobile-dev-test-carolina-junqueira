package model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Repository implements Parcelable {
    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String title;

    @SerializedName("description")
    private String description;

    @SerializedName("forks_count")
    private long fork;

    @SerializedName("stargazers_count")
    private long pointsAverage;

    @SerializedName("owner")
    private User user;

    @SerializedName("open_issues_count")
    private long openIssues;

    public Repository(int id, String title, String description, long fork, long pointsAverage, User user, long openIssues) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.fork = fork;
        this.pointsAverage = pointsAverage;
        this.user = user;
        this.openIssues = openIssues;
    }

    protected Repository(Parcel in) {
        id = in.readInt();
        title = in.readString();
        description = in.readString();
        fork = in.readLong();
        pointsAverage = in.readLong();
        user = in.readParcelable(User.class.getClassLoader());
        openIssues = in.readLong();
    }

    public static final Creator<Repository> CREATOR = new Creator<Repository>() {
        @Override
        public Repository createFromParcel(Parcel in) {
            return new Repository(in);
        }

        @Override
        public Repository[] newArray(int size) {
            return new Repository[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getFork() {
        return fork;
    }

    public void setFork(long fork) {
        this.fork = fork;
    }

    public long getPointsAverage() {
        return pointsAverage;
    }

    public void setPointsAverage(long pointsAverage) {
        this.pointsAverage = pointsAverage;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public long getOpenIssues() {
        return openIssues;
    }

    public void setOpenIssues(long openIssues) {
        this.openIssues = openIssues;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(title);
        parcel.writeString(description);
        parcel.writeLong(fork);
        parcel.writeLong(pointsAverage);
        parcel.writeParcelable(user, i);
        parcel.writeLong(openIssues);
    }
}
