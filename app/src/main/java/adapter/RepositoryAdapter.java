package adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.github_javapop.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import model.Repository;

public class RepositoryAdapter extends RecyclerView.Adapter<RepositoryAdapter.ViewHolder> {
    private List<Repository> repositories = new ArrayList<>();
    private Context context;

    public RepositoryAdapter(Context context) {
        this.context = context;
    }

    public void setRepositories(List<Repository> repositories) {
        if (repositories != null && repositories.size() > 0)
            this.repositories = repositories;
    }

    @Override
    public RepositoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_repository, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RepositoryAdapter.ViewHolder holder, int i) {
        Repository repos = repositories.get(i);

        holder.repository = repos;
        holder.repository_title.setText(repos.getTitle());

        if (repos.getDescription() != null) {
            holder.repository_desc.setText(repos.getDescription());
        } else {
            holder.repository_desc.setText(R.string.desc_text);
        }

        holder.repository_points.setText(String.valueOf(repos.getPointsAverage()));
        holder.repository_fork.setText(String.valueOf(repos.getFork()));
        holder.repository_login.setText(repos.getUser().getLogin());

        Picasso.with(context)
                .load(repos
                        .getUser()
                        .getPhoto())
                .resize(150, 150)
                .placeholder(R.drawable.ic_user)
                .into(holder.repository_photo);
    }

    @Override
    public int getItemCount() {
        return repositories.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        Repository repository;

        @BindView(R.id.repository_name)
        TextView repository_title;

        @BindView(R.id.repository_desc)
        TextView repository_desc;

        @BindView(R.id.repository_points_average)
        TextView repository_points;

        @BindView(R.id.repository_fork)
        TextView repository_fork;

        @BindView(R.id.repository_user)
        TextView repository_login;

        @BindView(R.id.repository_image)
        ImageView repository_photo;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}