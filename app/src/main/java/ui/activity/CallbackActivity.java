package ui.activity;

public interface CallbackActivity<T> {
    void onCreateComplete(T model);
}