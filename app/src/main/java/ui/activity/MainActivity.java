package ui.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.example.github_javapop.R;

import adapter.RepositoryAdapter;
import controller.RepositoryController;
import model.RepositoryResponse;

public class MainActivity extends AppCompatActivity implements CallbackActivity<RepositoryResponse> {
    private RepositoryController controller = new RepositoryController();
    private RepositoryAdapter adapter;
    private static ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView recyclerView = findViewById(R.id.recycler_view_list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        if (isConnected()) {
            progress = new ProgressDialog(this);
            progress.setMessage("Please wait...");
            progress.setCancelable(false);
            progress.show();

            controller.makeRetrofitCall(this);
        } else {
            new AlertDialog.Builder(this)
                    .setTitle(R.string.no_internet_message)
                    .setMessage(R.string.try_again_message)
                    .setPositiveButton(android.R.string.ok, null)
                    .setIcon(android.R.drawable.ic_dialog_alert).show();
        }

        adapter = new RepositoryAdapter(this);
        recyclerView.setAdapter(adapter);
    }

    private boolean isConnected() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager != null ? connectivityManager.getActiveNetworkInfo() : null;
        return networkInfo != null && networkInfo.isConnected();
    }

    @Override
    public void onCreateComplete(RepositoryResponse models) {
        if (progress != null) {
            progress.hide();
        }
        if (models.getRepositories() != null) {
            adapter.setRepositories(models.getRepositories());
            adapter.notifyDataSetChanged();
        } else {
            Toast.makeText(this, R.string.error_response, Toast.LENGTH_LONG).show();
        }
    }
}