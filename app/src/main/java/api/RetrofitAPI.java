package api;

import model.RepositoryResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RetrofitAPI {
    @GET("search/repositories")
    Call<RepositoryResponse> retrieveRepositories(
            @Query("q") String language,
            @Query("sort") String sort,
            @Query("page") int page
    );
}