package controller;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import api.CustomApplication;
import model.RepositoryResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ui.activity.CallbackActivity;

public class RepositoryController extends AppCompatActivity {

    public void makeRetrofitCall(final CallbackActivity<RepositoryResponse> activity) {
        final CustomApplication application = (CustomApplication) getApplication();
        application.getApi()
                .retrieveRepositories("Java", "stars", 1)
                .enqueue(new Callback<RepositoryResponse>() {
                    @Override
                    public void onResponse(Call<RepositoryResponse> call, Response<RepositoryResponse> response) {
                        if (response.isSuccessful()) {
                            activity.onCreateComplete(response.body());
                        } else {
                            activity.onCreateComplete(
                                    new RepositoryResponse(null));
                            Log.e("MAIN", "Error" + response.errorBody());
                        }
                    }

                    @Override
                    public void onFailure(Call<RepositoryResponse> call, Throwable t) {
                        activity.onCreateComplete(
                                new RepositoryResponse(null));
                        Log.e("MAIN", "Error", t);
                    }
                });
    }
}