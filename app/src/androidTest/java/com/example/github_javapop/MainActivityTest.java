package com.example.github_javapop;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import ui.activity.MainActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.allOf;

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity>
            activityRule = new ActivityTestRule<>(MainActivity.class, false, true);

    @Test
    public void whenActivityIsLaunched_shouldDisplay() {
        onView(allOf(withId(R.id.recycler_view_list), isDisplayed()));
        onView(allOf(withId(R.id.repository_view), isDisplayed()));
    }

    @Test
    public void checkRepositoryItem_isDisplayed() {
        onView(allOf(withId(R.id.repository_name), isDisplayed()));
        onView(allOf(withId(R.id.repository_desc), isDisplayed()));
        onView(allOf(withId(R.id.repository_user), isDisplayed()));
        onView(allOf(withId(R.id.repository_fork), isDisplayed()));
        onView(allOf(withId(R.id.repository_points_average), isDisplayed()));
        onView(allOf(withId(R.id.repository_image), isDisplayed()));
    }
}